@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title mb-3">{{$post->title}}</h2>
            <p class="card-text text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            <div class="mt-3">
                <a href="/posts" class="card-link">View all posts</a>
            </div>
        </div>
    </div>
@endsection