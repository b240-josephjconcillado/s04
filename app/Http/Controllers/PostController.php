<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    //
    public function create()
    {
        if(Auth::user()){
            return view('posts.create');
        } else {
            return redirect('/login');
        }
    }

    public function store(Request $request)
    {
        if(Auth::user()){
            // create a new Post Object from the post model
            $post = new Post;

            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = Auth::user()->id;
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    public function index()
    {
        //get all posts from the database
        $posts = Post::where('isActive',true)->get();
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {
        $posts = Post::inRandomOrder()
                ->limit(3)
                ->where('isActive',true)
                ->get();

        return view('welcome')->with('posts', $posts); 
    }

    public function myPosts()
    {
        if(Auth::user()){
            $posts = Auth::user()->posts->where('isActive',true);
            return view('posts.index')->with('posts',$posts);
        } else {
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post',$post);
    }

    public function edit($id)
    {
        if(Auth::user()){
            $post = Post::find($id);
            return view('posts.edit')->with('post',$post);
        } else {
            return redirect('/login');
        }
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        
        if(Auth::user()->id == $post->user_id){
            // create a new Post Object from the post model

            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();

            echo '<script>alert("Successfully updated!"); window.location.href="/posts";</script> ';

            // return redirect('/posts');
        } else {
            return redirect('/posts');
        }
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        
        if(Auth::user()->id == $post->user_id){
            $post->delete();
            echo '<script>alert("Successfully deleted"); window.location.href="/posts";</script>';

        } else {
            return redirect('/posts');
        }
    }

    public function archive($id)
    {
        $post = Post::find($id);
        
        if(Auth::user()->id == $post->user_id){
            // $post->delete();
            $post->isActive = false;
            $post->save();
            echo '<script>alert("Successfully archived"); window.location.href="/posts";</script>';

        } else {
            return redirect('/posts');
        }
    }

}
